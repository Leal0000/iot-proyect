import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './vistas/login/login.component';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { AddComponent } from './vistas/add/add.component';
import { CodeComponent } from './vistas/code/code.component';
import { DataComponent } from './vistas/data/data.component';
import { AgregarComponent } from './vistas/agregar/agregar.component';
import { ListadoComponent } from './vistas/listado/listado.component';
import { EditaComponent } from './vistas/edita/edita.component';

const routes: Routes = [
  {path:'', redirectTo:'login', pathMatch:'full'},
  {path:'login', component:LoginComponent},
  {path:'dashboard', component:DashboardComponent},
  {path:'crear', component:AddComponent},
  {path:'code/:email', component:CodeComponent},
  {path:'data', component:DataComponent},
  {path:'agregar', component:AgregarComponent},
  {path:'listado', component:ListadoComponent},
  {path:'edita/:id/:tipo', component:EditaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent, DashboardComponent, AddComponent, CodeComponent, DataComponent, AgregarComponent, ListadoComponent, EditaComponent]