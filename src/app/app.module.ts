import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './vistas/login/login.component';
import { FooterComponent } from './plantillas/footer/footer.component';
import { HeaderComponent } from './plantillas/header/header.component';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AddComponent } from './vistas/add/add.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { GoogleChartsModule } from 'angular-google-charts';
import { CodeComponent } from './vistas/code/code.component';
import { DataComponent } from './vistas/data/data.component';
import { GraficasComponent } from './vistas/graficas/graficas.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgregarComponent } from './vistas/agregar/agregar.component';
import { PlantillaComponent } from './vistas/plantilla/plantilla.component';
import { ListadoComponent } from './vistas/listado/listado.component';
import { EditaComponent } from './vistas/edita/edita.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    HeaderComponent,
    DashboardComponent, 
    routingComponents, AddComponent, CodeComponent, DataComponent, GraficasComponent, AgregarComponent, PlantillaComponent, ListadoComponent, EditaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    NgxChartsModule,
    GoogleChartsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
