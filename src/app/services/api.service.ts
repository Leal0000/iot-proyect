import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { responseI } from '../models/response.interface';
import { loginI } from '../models/login.interface';
import { statusI } from '../models/state.interface';
import { Observable } from 'rxjs';
import { stI } from '../models/st.interface';
import { TempI } from '../models/temperatura.interface';
import { valorI } from '../models/valor.interface';
import { vaI } from '../models/va.interface';
import { IluminacionI } from '../models/iluminacion.interface';
import { IluI } from '../models/ilum.interface';
import { code } from '../models/code.interface';
import { userI } from '../models/user.interface';
import { allI } from '../models/all.interface';
import { tokenI } from '../models/token.interface';
import { deviceI } from '../models/device.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  url:string="https://heroku-flas.herokuapp.com/";

  constructor(private http:HttpClient) { }
  
  loginUser(form:loginI):Observable<responseI>{
    let direccion = this.url + "api/login";
    return this.http.post<responseI>(direccion, form);
  }

  createUser(form:loginI):Observable<responseI>{
    let direccion = this.url + "api/register";
    return this.http.post<responseI>(direccion, form);
  }

  createTokennew(form:tokenI):Observable<responseI>{
    let direccion = this.url + "api/add_users_devices";
    return this.http.post<responseI>(direccion, form)
  }

  codeUser(form:code):Observable<responseI>{
    let direccion = this.url + "api/code";
    return this.http.post<responseI>(direccion, form);
  }

  userInformation(form:loginI):Observable<userI>{
    let direccion = this.url + 'api/person';
    return this.http.post<userI>(direccion,form);
  }
  
  state(id:any):Observable<statusI>{
    let direccion = this.url + "api/device/" + id;
    return this.http.get<statusI>(direccion);
  }

  updateState(form:stI, id:any):Observable<responseI>{
    let direccion = this.url + "api/device/" + id;
    return this.http.put<responseI>(direccion, form); 
  }

  updatenewState(form:stI, id:any):Observable<responseI>{
    let direccion = this.url + "api/device_status/" + id;
    return this.http.put<responseI>(direccion, form); 
  }


  updateValor(form:vaI, id:number):Observable<responseI>{
    let direccion = this.url + "api/intensidad/" + id;
    return this.http.put<responseI>(direccion, form);
  }
  updateIluminacion(form:IluI, id:number):Observable<responseI>{
    let direccion = this.url + "api/iluminacion_rgb/" + id;
    return this.http.put<responseI>(direccion, form);
  }
  updatenewIluminacion(form:IluI, id:number):Observable<responseI>{
    let direccion = this.url + "api/device_color/" + id;
    return this.http.put<responseI>(direccion, form);
  }

  updatenewname(form:deviceI,id:any,tipo:any):Observable<responseI>{
    let direccion = this.url + 'api/edit_users_devices/' + id + '/' + tipo
    return this.http.put<responseI>(direccion,form);
  }
  getTemperature():Observable<TempI>{
    let direccion = this.url + "temperatura";
    return this.http.get<TempI>(direccion);
  }
  getTemperatureReal():Observable<TempI>{
    let direccion = this.url + "temperatura_real";
    return this.http.get<TempI>(direccion);
  }
  getValor(id:number):Observable<valorI>{
    let direccion = this.url + "api/intensidad/" + id;
    return this.http.get<valorI>(direccion);
  }
  getIluminacion(id:number):Observable<IluminacionI>{
    let direccion = this.url + "api/iluminacion_rgb/" + id;
    return this.http.get<IluminacionI>(direccion);
  }

  getDeviceonly(id:number):Observable<deviceI>{
    let direccion = this.url + 'api/get_user_device__on/' + id
    return this.http.get<deviceI>(direccion)
  }

  getDevices(id:any):Observable<allI>{
    let direccion = this.url + "api/search_users_devices/" + id
    return this.http.get<allI>(direccion);
  }

}
