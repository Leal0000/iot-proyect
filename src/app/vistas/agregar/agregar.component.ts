import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tokenI } from 'src/app/models/token.interface';
import { AlertasService } from 'src/app/services/alertas.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  body:string="sb-nav-fixd";
  user:any;

  tokenForm = new FormGroup({
    user : new FormControl('',Validators.required),
    token : new FormControl('',Validators.required)
  })

  constructor(private router:Router, private api:ApiService, private alertasService:AlertasService) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('id_user')
    this.tokenForm.setValue({
      'user' : this.user,
      'token' : ""
    });
  }

  barra() {
    if (localStorage.getItem('sb|sidebar-toggle') == "true") {
      this.body = "sb-nav-fixd";
      localStorage.setItem('sb|sidebar-toggle', 'false');
    }
    else {
      this.body = "sb-sidenav-toggled";
      localStorage.setItem('sb|sidebar-toggle', 'true');
    }
  }

  data(){
    this.router.navigate(['data']);
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
  agregar(){
    this.router.navigate(['agregar']);
  }
  dashboard(){
    this.router.navigate(['dashboard']);
  }

  addtoken(form:tokenI){
    this.api.createTokennew(form).subscribe(data=>{
      console.log(data);
      if(data.status == "true"){
        this.alertasService.showSuccess(data.response, 'Confirmado ' + data.dispositivo);
      }
      else{
        if(data.dispositivo){
          this.alertasService.showError(data.response, 'Error con ' + data.dispositivo);
        }
        else{

        }
        this.alertasService.showError(data.response, 'Error');
      }
      
    });
  }

}
