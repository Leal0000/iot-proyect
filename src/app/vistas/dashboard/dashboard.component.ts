import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/services/alertas.service';
import { ApiService } from 'src/app/services/api.service';
import { statusI } from 'src/app/models/state.interface';
import { stI } from 'src/app/models/st.interface';
import { TempI } from 'src/app/models/temperatura.interface';
import { vaI } from 'src/app/models/va.interface';
import { IluI } from 'src/app/models/ilum.interface';
declare var jQuery:any;
declare var $:any;
/*
abierto: sb-nav-fixd
cerrado= sb-sidenav-toggled
*/ 
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  id: any = "";
  body:string="sb-nav-fixd";
  state: any = "";
  status_devices:any="";
  device_alls:any="";
  data_temp: any = [];
  color_proof:any=[];
  valor: number = 0;
  hexa: any = "";
  color_hexa : any = "";
  color: any = "";
  message:any="";
  public colores:Array<any>=[]
  public colores_final:Array<any>=[]
  public array1:Array<any>=[]
  public array2:Array<any>=[]
  public array3:Array<any>=[]
  public array4:Array<any>=[]




  formState = new FormGroup({
    state: new FormControl('')
  });

  formVal = new FormGroup({
    valor: new FormControl('')
  })
  
  formIlu = new FormGroup({
    red: new FormControl(''),
    green: new FormControl(''),
    blue: new FormControl(''),
  })

  constructor(private api: ApiService, private alerta: AlertasService, private router: Router) {

  }

  ngOnInit(): void {
    if(localStorage.getItem('token')){
      this.getState();
      this.getValor();
      this.getIluminacion();
      localStorage.setItem('sb|sidebar-toggle', 'true');
      this.barra();
      this.devices(); 
    }
    else{
      this.router.navigate(['login'])
    }
  }

  viewid(name : any, all : any){
    console.log(name);
    console.log(all);
  }
//principales
  barra() {
    if (localStorage.getItem('sb|sidebar-toggle') == "true") {
      this.body = "sb-nav-fixd";
      localStorage.setItem('sb|sidebar-toggle', 'false');
    }
    else {
      this.body = "sb-sidenav-toggled";
      localStorage.setItem('sb|sidebar-toggle', 'true');
    }
  }

  data(){
    this.router.navigate(['data']);
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
  agregar(){
    this.router.navigate(['agregar']);
  }
  dashboard(){
    this.router.navigate(['dashboard']);
  }

  edita(id:any,tipo:any){
    this.router.navigate(['edita',id,tipo]);
  }

  listado(){
    this.router.navigate(['listado']);
  }
  
//principales
  devices(){
    let id = localStorage.getItem('id_user');
    //let id = 3;
    this.api.getDevices(id).subscribe((data: any) =>{
      console.log(data);
      if(data.status == "true"){
        console.log(data);
        this.status_devices = data.status;
        this.array1 = data.dispositivos;
        this.array2 = data.states;
        this.array3 = data.colors;
        this.array4 = data.cortinas;
        console.log(this.array3)
        this.colores = []
        for(let i =0; i<this.array3.length; i++){
          this.colores = [{
            "id" : this.array3[i]['device_id'],
            "color" : this.rgbToHex(this.array3[i]['red'],this.array3[i]['green'],this.array3[i]['blue'])
          }]
          this.colores_final.push(this.colores);
        }
        console.log(this.colores_final[0][0]['color']);
        //console.log("Colores:" + this.colores[0].id);
      }
      else{
        this.status_devices = data.status;
        this.message = data.dispositivos;
      }
    })
  }

  getState() {
    let id = localStorage.getItem('id_device');
    this.api.state(id).subscribe(data => {
      console.log(data);
      if (data.state == true) {
        this.state = "checked";
      }
      else {
        this.state = ""
        this.id = id;
      }
    });
  }
  getIluminacion(){
    let id = Number(localStorage.getItem('id_device'));
    let arrayRGB:any = [];
    this.api.getIluminacion(id).subscribe(data => {
      console.log(data); 
      let hexa_rgb  = this.rgbToHex(data.red, data.green, data.blue);
      console.log(hexa_rgb);
      this.hexa = hexa_rgb;
    });

  }

  updateState(event: Event) {
    let id = localStorage.getItem('id_device');
    if ((<HTMLInputElement>event.target).checked) {
      console.log("si");
      let sta: stI;
      this.formState.setValue({
        'state': true
      });
      sta = this.formState.value
      this.api.updateState(sta, id).subscribe(data => {
        console.log(data);
        this.alerta.showSuccess("Lampara encendida", "Successfully")
      });
    }
    else {
      console.log("no");
      let sta: stI;
      this.formState.setValue({
        'state': false
      });
      sta = this.formState.value
      this.api.updateState(sta, id).subscribe(data => {
        console.log(data);
        this.alerta.showSuccess("Lampara apagada", "Successfully")

      });
    }
  }


  updatenewState(event: Event, id_d:any, name:any) {
    //let id = localStorage.getItem('id_device');
    //let id = localStorage.getItem('id_user');
    let id = id_d;
    let nombre = name;
    console.log(id);
    if ((<HTMLInputElement>event.target).checked) {
      console.log("si");
      let sta: stI;
      this.formState.setValue({
        'state': true
      });
      sta = this.formState.value
      this.api.updatenewState(sta, id).subscribe(data => {
        console.log(data);
        this.alerta.showSuccess("Lampara " + nombre + " Encendida", "Successfully")
      });
    }
    else {
      console.log("no");
      let sta: stI;
      this.formState.setValue({
        'state': false
      });
      sta = this.formState.value
      this.api.updatenewState(sta, id).subscribe(data => {
        console.log(data);
        this.alerta.showSuccess("Lampara " + nombre + " Apagada", "Successfully")

      });
    }
  }





  updateValor(event: Event) {
    let id = Number(localStorage.getItem('id_device'));
    let va = this.valor
    if ((<HTMLInputElement>event.target).value != String(va)) {
      console.log("si");
      let val: vaI;
      this.formVal.setValue({
        'valor': (<HTMLInputElement>event.target).value
      });
      val = this.formVal.value
      this.api.updateValor(val, id).subscribe(data => {
        console.log(data);
        let msga = "Intensidad modificada al " + (<HTMLInputElement>event.target).value + "%";
        this.alerta.showSuccess(msga, "Succesfully");
      });
    }
  }

updatenewIluminacion(idd:any, event:Event){
    let id = idd
    let valor_color = (document.getElementById(idd) as HTMLInputElement).value;
    console.log(valor_color);
    let valor_rbg: any = this.hexToRgb(valor_color);
    let red: number = 0;
    let green: number = 0;
    let blue: number = 0;
    let arregloDeArreglos: any = [];
    console.log(valor_rbg);
    for (let i = 0; i < valor_rbg.length; i += 1) {
      let pedazo = valor_rbg.slice(i, i + 1);
      arregloDeArreglos.push(pedazo);
      if (i == 0) {
        red = pedazo;
        console.log("red:" + red);
      }
      if (i == 1) {
        green = pedazo;
        console.log("green:" + green);
      }
      if (i == 2) {
        blue = pedazo;
        console.log("blue:" + blue);
      }
    }
    let Ilum : IluI;
    //let id : number = Number(localStorage.getItem('id_device'));
    this.formIlu.setValue({
      'red' : Number(red),
      'green' : Number(green),
      'blue' : Number(blue),
    });
    Ilum = this.formIlu.value
    this.api.updatenewIluminacion(Ilum, id).subscribe(data =>{
      let msga = data.response;
      this.alerta.showSuccess(msga, "Succesfully");
    });
  }

  updateIluminacion(){
    let valor_color = this.hexa
    console.log(valor_color);
    let valor_rbg: any = this.hexToRgb(valor_color);
    let red: number = 0;
    let green: number = 0;
    let blue: number = 0;
    let arregloDeArreglos: any = [];
    console.log(valor_rbg);
    for (let i = 0; i < valor_rbg.length; i += 1) {
      let pedazo = valor_rbg.slice(i, i + 1);
      arregloDeArreglos.push(pedazo);
      if (i == 0) {
        red = pedazo;
        console.log("red:" + red);
      }
      if (i == 1) {
        green = pedazo;
        console.log("green:" + green);
      }
      if (i == 2) {
        blue = pedazo;
        console.log("blue:" + blue);
      }
    }
    let Ilum : IluI;
    let id : number = Number(localStorage.getItem('id_device'));
    this.formIlu.setValue({
      'red' : Number(red),
      'green' : Number(green),
      'blue' : Number(blue),
    });
    Ilum = this.formIlu.value
    this.api.updateIluminacion(Ilum, id).subscribe(data =>{
      let msga = data.response;
      this.alerta.showSuccess(msga, "Succesfully");
    });
  }

/*
viewValor(event: Event) {
    let valor_color = (<HTMLInputElement>event.target).value
    console.log(valor_color);
    let valor_rbg: any = this.hexToRgb(valor_color);
    let red: number = 0;
    let green: number = 0;
    let blue: number = 0;
    let arregloDeArreglos: any = [];
    console.log(valor_rbg);
    for (let i = 0; i < valor_rbg.length; i += 1) {
      let pedazo = valor_rbg.slice(i, i + 1);
      arregloDeArreglos.push(pedazo);
      if (i == 0) {
        red = pedazo;
        console.log("red:" + red);
      }
      if (i == 1) {
        green = pedazo;
        console.log("green:" + green);
      }
      if (i == 2) {
        blue = pedazo;
        console.log("blue:" + blue);
      }
    }
    console.log("Arreglo de arreglos: ", arregloDeArreglos);
  }*/



  getValor() {
    let id = Number(localStorage.getItem('id_device'));
    this.api.getValor(id).subscribe(data => {
      console.log(data);
      this.valor = data.valor;
    });
  }

  hexToRgb(hex: any) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m: any, r: any, g: any, b: any) {
      return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? [
      parseInt(result[1], 16),
      parseInt(result[2], 16),
      parseInt(result[3], 16)
    ] : null;
  }

  rgbToHex(r:number, g:number, b:number) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
  }

}


















