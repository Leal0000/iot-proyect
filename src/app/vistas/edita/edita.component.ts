import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { deviceI } from 'src/app/models/device.interface';
import { AlertasService } from 'src/app/services/alertas.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-edita',
  templateUrl: './edita.component.html',
  styleUrls: ['./edita.component.css']
})
export class EditaComponent implements OnInit {
  id:any;
  tipo:any;
  body:string="sb-nav-fixd";

  editaForm = new FormGroup({
    name:new FormControl('', Validators.required)
  });


  constructor(private router:Router, private api:ApiService, private alertasService:AlertasService, private activerouter:ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.activerouter.snapshot.paramMap.get('id');
    this.tipo = this.activerouter.snapshot.paramMap.get('tipo');    
    this.getname();
   
  }
  barra() {
    if (localStorage.getItem('sb|sidebar-toggle') == "true") {
      this.body = "sb-nav-fixd";
      localStorage.setItem('sb|sidebar-toggle', 'false');
    }
    else {
      this.body = "sb-sidenav-toggled";
      localStorage.setItem('sb|sidebar-toggle', 'true');
    }
  }

  data(){
    this.router.navigate(['data']);
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
  agregar(){
    this.router.navigate(['agregar']);
  }
  dashboard(){
    this.router.navigate(['dashboard']);
  }

  edita(id:any,tipo:any){
    this.router.navigate(['edita',id,tipo]);
  }

  listado(){
    this.router.navigate(['listado']);
  }

  getname(){
    this.api.getDeviceonly(this.id).subscribe(data =>{
      console.log(data.nombre);
      this.editaForm.setValue({
      'name' : data.nombre
      })
    })
  }

  editname(form:deviceI){
    this.api.updatenewname(form, this.id, this.tipo).subscribe(data=>{
      console.log(data);
      if(data.status == "true"){
        this.alertasService.showSuccess(data.response, 'Actualizado');
      }
      else{
        this.alertasService.showError(data.response, 'Error');
      }
    });
  }

  

}
