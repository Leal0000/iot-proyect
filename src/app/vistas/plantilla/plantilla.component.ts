import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/services/alertas.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-plantilla',
  templateUrl: './plantilla.component.html',
  styleUrls: ['./plantilla.component.css']
})
export class PlantillaComponent implements OnInit {

  body:string="sb-nav-fixd";

  constructor(private router:Router, private api:ApiService, private alertasService:AlertasService) { }

  ngOnInit(): void {
  }

  //principales
  barra() {
    if (localStorage.getItem('sb|sidebar-toggle') == "true") {
      this.body = "sb-nav-fixd";
      localStorage.setItem('sb|sidebar-toggle', 'false');
    }
    else {
      this.body = "sb-sidenav-toggled";
      localStorage.setItem('sb|sidebar-toggle', 'true');
    }
  }

  data(){
    this.router.navigate(['data']);
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }
  agregar(){
    this.router.navigate(['agregar']);
  }
  dashboard(){
    this.router.navigate(['dashboard']);
  }

  listado(){
    this.router.navigate(['listado']);
  }
  
//principales

}
